import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:expenses/pages/my-home-page.dart';

main() => runApp(ExpansesApp()); 

class ExpansesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
      theme: ThemeData(
        primarySwatch: Colors.purple,
        accentColor: Colors.amber,
        fontFamily: 'Quicksand', // * font que vai ser aplicado em toda aplicação
        appBarTheme: AppBarTheme( // * modificando style do appBar
          textTheme: ThemeData.light().textTheme.copyWith( // * ThemeData.light() é o tema padrão do flutter
            title: TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.bold
            ), 
          ),
        ),
      ),
    );
  }
}
