import 'dart:math';

import 'package:expenses/components/chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:expenses/models/transaction.dart';

import 'package:expenses/components/transaction-form.dart';
import 'package:expenses/components/transaction-list.dart';


class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final List<Transaction> _transaction = [
    Transaction(
      id: 't0',
      title: 'Conta Antiga',
      value: 400.00,
      date: DateTime.now().subtract(Duration(days: 33))
    ),
    Transaction(
      id: 't1',
      title: 'Novo Tênis da Corrida',
      value: 310.76,
      date: DateTime.now().subtract(Duration(days: 3))
    ),
    Transaction(
      id: 't2',
      title: 'Conta de Luz',
      value: 211.30,
      date: DateTime.now().subtract(Duration(days: 4))
    )
  ];

  List<Transaction> get _recentTransaction {
    // * filtra os registros com datas de 7 dias atras
    return _transaction.where((tr) {
      return tr.date.isAfter(DateTime.now().subtract(
        Duration(days: 7)
      ));
    }).toList();
  }

  _openTransactionModal(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return TransactionForm(_addTransaction);
      }
    );
  }

  _addTransaction(String title, double value) {
    final newTransaction = Transaction(
      id: Random().nextDouble().toString(),
      title: title,
      value: value,
      date: DateTime.now()
    );

    setState(() {
      _transaction.add(newTransaction);
    });

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Navigatio Bar'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => _openTransactionModal(context),
          ),
        ],
      ),
      body: SingleChildScrollView(
              child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch, // Alinhando os textos a esquerda
          children: <Widget>[
            Chart(_recentTransaction),
            TransactionList(_transaction)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed:() => _openTransactionModal(context)
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
           BottomNavigationBarItem(
              icon: Icon(Icons.money_off), title: Text('Despesas')),
          BottomNavigationBarItem(
              icon: Icon(Icons.local_atm), title: Text('Receitas')),
          BottomNavigationBarItem(
              icon: Icon(Icons.monetization_on), title: Text('Investimentos'))
        ],
      ),
    );
  }
}
