import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:intl/intl.dart';

import 'package:expenses/models/transaction.dart';

import 'package:expenses/components/transaction-empty.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;

  TransactionList(this.transactions);

/**
 * * ListView server para criar uma lista de items de tamanho dinamico, onde não se sabe o tamanho exato 
 * * da lista, pois os items são renderizados de acordo com scroll
 */
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: transactions.isEmpty ? TransactionEmpety() : ListView.builder(
        itemCount: transactions.length,
        itemBuilder: (ctx, index) {
          final tr = transactions[index];

          return Card(
              child: Row(children: <Widget>[
              Container(
                  //* Container pode ser utilizado para estilizar os elementos
                  margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 2)),
                      padding: EdgeInsets.all(10),
                      child: Text(
                    /**
                        ** $ é caracater especial \ server para realizar escape,
                        ** informando que quero utilizar o literal
                        */
                    'R\$ ${tr.value}',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Theme.of(context).primaryColor  // * as cores dos elementos vai ser definida de acordo com a cor defenida no ThemeData()
                    ),
                  )),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    tr.title,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    DateFormat('d MMM y').format(tr.date),
                    style: TextStyle(color: Colors.grey),
                  )
                ])
          ]));
        },
      ),
    );
  }
}
