import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
 
  final String label;
  final double value;
  final double percentage;

  ChartBar({
    this.label,
    this.value,
    this.percentage
   });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        FittedBox( // o conteudo vai se adequar ao local onde esta inserido
          child: Text('${value.toStringAsPrecision(2)}')
        ),
        SizedBox(height: 5),
        Container(
          height: 60,
          width: 10,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                decoration: BoxDecoration( //* gerando barra vertical
                  border: Border.all(
                    color: Colors.grey,
                    width: 1.0
                  ),
                  color: Color.fromARGB(220, 220, 220, 1),
                  borderRadius: BorderRadius.circular(7)
                ),
              ),
              FractionallySizedBox( //* gerando barra vertical interna para exibir o percentual carregado
                heightFactor: percentage,
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(5)
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(height: 5),
        Text(label)
      ],
    );
  }
}
