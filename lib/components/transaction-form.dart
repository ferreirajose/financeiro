import 'package:flutter/material.dart';

class TransactionForm extends StatefulWidget {

  final void Function(String, double) onSubmit; 

  TransactionForm(this.onSubmit);

  @override
  _TransactionFormState createState() => _TransactionFormState();
}

class _TransactionFormState extends State<TransactionForm> {
  final titleControler = TextEditingController();

  final valueControler = TextEditingController();

  void _submitForm() {
    final title = titleControler.text;
    final value  = double.tryParse(valueControler.text ?? 0.0); //* tentar converter para double, ou retorna 0.0

    if (title.isEmpty || value <= 0) {
      return;
    }

    /**
     ** onSubmit método esta sendo passado por herança, pois a class _TransactionFormState extends State<TransactionForm>
     ** visto que _TransactionFormState não tem esse método, ele esta prensente em TransactionForm
     */
    widget.onSubmit(title, value);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            TextField(
                controller: titleControler,
                onSubmitted: (_) => _submitForm(), //* (_) indicar que o parametro não sera utilizado
                decoration: InputDecoration(labelText: 'Titulo')),
            TextField(
                controller: valueControler,
                keyboardType: TextInputType.numberWithOptions(decimal: true), //* teclando apenas com numeros, a opção decimal = true é para o IOS que não aprenseta pontual decimal apenas os números
                onSubmitted: (_) => _submitForm(), //* (_) indicar que o parametro não sera utilizado
                decoration: InputDecoration(labelText: 'Valor (R\$)')),
            Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
              FlatButton(
                  child: Text('Nova transção'),
                  textColor: Theme.of(context).primaryColor, // * as cores dos elementos vai ser definida de acordo com a cor defenida no ThemeData()
                  onPressed: _submitForm
              )
            ])
          ],
        ),
      ),
    );
  }
}
